
# -*- coding: utf-8 -*-
"""
Created on Mon Sep  7 18:36:31 2020

@author: dell
"""

import datetime
import requests
import Rooms
import openpyxl
import Messages
import initializing
import Scheduling
from queue import Queue
from threading import Thread 

"""
code 2 - prices for an options info[0], info[1], info[2] prices
code 3 - exchange between two operations info[0]-id op1, info[1]-id op2, info[2] -price
code 4 - cancel this operation info[0]-id of op, info[1] - price (in case the HOW wants specific surgeon for this operations)
code 5 - adding operation for combination instead some operation info[0]-id_op, info[1] - price
*if the price is -1 then this is a hard constraint
"""
class Constraint:
    def __init__(self,id_code, info):
        self.id_code = id_code
        self.info = info

class Option:
    def __init__(self, date, unit, requests):
        self.date = date
        self.unit = unit
        self.requests = requests 
    
    def add_request(self, request):
        self.requests.append(request)
    def get_dict(self):
        dic = {}
        dic["date"] = self.date
        dic["unit"] = self.unit
        dic["request"] = self.requests
        return dic

        
    
    
    def __str__(self):
        string = ' '
        for p in self.requests:
            string =' '.join([string, str(p.id_p)+","])
        return str(self.date)+str(self.unit)+" patients:"+string
        
class Prices:
    def __init__(self):
        self.prices = {}
        self.prices[0]=0
        self.prices[1]=0
        self.prices[2]=0
        
    def set_head_ward_price(self, price):
        self.prices[0] = price
    def set_mangement_price(self, price):
        self.prices[1] = price
    def set_coordinator_price(self, price):
        self.prices[2] = price
    
    def get_average(self):
        if self.prices[0] == -1 or self.prices[1]==-1 or self.prices[2]==-1:
            return -1
        return (self.prices[0]+self.prices[1]+self.prices[2])/3


class Head_ward:
    def __init__(self, num_ward):
        self.num_ward = num_ward
        self.dict_src = Scheduling.Communication.dict_src
        
 #getting request that need restrict for their domain, return - dict: {patient_id:date_domain}       
    def producer(self, requests):
        # print("i am how")
        # # with self.cv:
        # #     while self.queue_how.empty():
        # #         self.cv.wait()
        # #     print("here ", self.queue_how.get())
        # Scheduling.global_queue.put("hello from how")
        # print("sending messages")
        dictionary = {}
        for patient in requests:
            if (len(str(patient.surgeon))>0 and patient.surgeon!= None):
                surgeon = Surgeon(self.num_ward,patient.surgeon)
                dictionary[patient.id_p] = surgeon.producer(patient)
            elif ( (len(str(patient.deadline))>0 and patient.deadline!=None) or (len(str(patient.from_date))>0)and patient.from_date!=None) :
                dictionary[patient.id_p] =self.set_specific_domain(patient)
        msg = Messages.Msg(0,self.dict_src["HoW"],self.dict_src["CA"],dictionary )
        return msg 
    
    
    
    def set_specific_domain(self,patient):
        dates_list = []
        end_time = datetime.datetime(2020, 7, 30)
        st_time = datetime.datetime(2020, 7, 1)
        if len(str(patient.deadline))>0 and patient.deadline!=None :
            end_time = patient.deadline
        if len(str(patient.from_date))>0 and patient.from_date!=None:
            st_time = patient.from_date
        while st_time <= end_time:
            dates_list.append(st_time.strftime("%Y-%m-%d %H:%M:%S"))
            st_time = st_time + datetime.timedelta(hours=24)
        #print(patient.id_p, "dates: ")
        # print(dates_list)
        # print()

        return dates_list
    
    
    def get_src(self):
        return Messages.Src.HOW
    
    def pricing(self, request, date):
        price_d = 0
        price_complexity = 0
        price_surgeons = 0
        if (len(str(request.deadline))>0 and request.deadline!=None):
            delta = datetime.datetime(2021, 4, 1) - request.deadline
            price_d += delta.days*0.5
        elif request.urgency == 3 and date<=datetime.datetime(2021, 4, 1) and date>=datetime.datetime(2021, 4, 10) :
            price_d = 0
        elif request.urgency == 2 and date<=datetime.datetime(2021, 4, 1) and date>=datetime.datetime(2021, 4, 10):
            price_d = 5
        else:
            delta = datetime.datetime(2021, 4, 1) -request.arrival
            price_d += delta.days*0.5
        
        #price_surgeons += self.get_price_by_surgeon(request, date)
        
        if price_surgeons == 100 or price_d==100:
            return 100
       # print(request.id_p, "How : ",0.4*price_d + 0.25*price_complexity + 0.35*price_surgeons)
        return 0
        return (0.4*price_d + 0.25*price_complexity + 0.35*price_surgeons)
    
    
    
    def get_price_by_surgeon(self, request, date):
        s1 = Surgeon(2,self.surgeons[Rooms.Unit.Bariatric][0][0])
        s2 = Surgeon(2,self.surgeons[Rooms.Unit.Bariatric][1][0])
        if request.complexity == 3:
            if date in s1.shifts_date:
                return 0
            else:
                return 100
        elif request.complexity == 2:
            if date in s2.shifts_date:
                return 0
            else:
                return 12
        if date in s2.shifts_date:
                return 0
        else:
                return 7
    
        
    def calculate_price_op(self, option):
        price = 0
        
        #print("here in calculate price op: ", option)
        for req in option.requests:
            price += self.pricing(req, option.date)
            
            
        if len(option.requests)< Rooms.get_num_by_unit(option.unit):
            price += 25 * (Rooms.get_num_by_unit(option.unit) - len(option.requests))
        
        if price >= 100:
            #print("here in pricing op HOW")
            return 100
        return price
                
    def response_with_pricing(self, options,remaining_requests):
        prices = {}
        for i in range(0,3):
            prices[i] = self.calculate_price_op(options[i])
        return prices
            
        
    
    #def response_with_pricing(self, msg):
    
    #def get_price_by_surgeon(self, surgeons, date):
                
   
        
    
    


class Management:
    def __init__(self, num_ward, allocation):
        self.num_ward = num_ward
        self.dict = {}
        self.dict[Rooms.Unit.Bariatric] = []
        self.dict[Rooms.Unit.Activity_Room] = []
        self.dict[Rooms.Unit.Proctology] = []
        self.dict[Rooms.Unit.Colon] = []
        self.dict[Rooms.Unit.Hospitalization_Day] = []
        self.dict[Rooms.Unit.General] = []
        self.dict_src = Scheduling.Communication.dict_src
        
        self.set_dates(allocation)
             
    def set_dates(self, allocation_rooms):
        for date in allocation_rooms:
            for room in allocation_rooms[date]:
                if room.unit1 == Rooms.Unit.Bariatric:
                     self.dict[Rooms.Unit.Bariatric].append(date)
                if room.unit1 == Rooms.Unit.Activity_Room:
                     self.dict[Rooms.Unit.Activity_Room].append(date)
                if room.unit1 == Rooms.Unit.Proctology:
                     self.dict[Rooms.Unit.Proctology].append(date)
                if room.unit1 == Rooms.Unit.Colon:
                     self.dict[Rooms.Unit.Colon].append(date)
                if room.unit1 == Rooms.Unit.Hospitalization_Day:
                     self.dict[Rooms.Unit.Hospitalization_Day].append(date)
                if room.unit1 == Rooms.Unit.General:
                     self.dict[Rooms.Unit.General].append(date)
    
    #gets request fillng date domain by rooms allocations
    def producer(self,patients):
        domain_dates = {}
        for unit in patients:
            for key in patients[unit]:
                patient = patients[unit][key]
                #patient.mangement_queue_add(self.dict[patient.unit])
                domain_dates[patient.id_p] = [date_obj.strftime("%Y-%m-%d %H:%M:%S") for date_obj in self.dict[patient.unit]]
        msg = Messages.Msg(0,self.dict_src["ORM"],self.dict_src["CA"],domain_dates )
        return msg
                
                

    
    def get_dates_by_unit(self):
        return self.dict
    def get_src(self):
        return Messages.Src.ORM    
    


class Surgeon:
    def __init__(self, num_ward, id_s):
        self.id_s = id_s
        self.shifts_date = []
        
    def get_shifts_dates(self):
        URL = "https://server-soroka-demo.herokuapp.com/shifts_seniors/get_by_senior_id"
        #print(self.surgeon)
        #num = int(int(self.surgeon))
        PARAMS = {"senior_id": self.id_s}
        r = requests.get(url = URL, json = PARAMS) 
        data = r.json()
        
        for i in range(len(data)):
            date, __ = data[i]['st_time'].split('T')
            self.shifts_date.append(datetime.datetime.strptime(date, '%Y-%m-%d'))
    
    def producer(self,patient):
        end_time = datetime.datetime(2021, 4, 30)
        st_time = datetime.datetime(2021, 4, 1)
        if len(str(patient.deadline))>0 and patient.deadline!=None :
            end_time = patient.deadline
        if len(str(patient.from_date))>0 and patient.from_date!=None :
            st_time = patient.from_date
        dates_list = []
        for date_time_obj in self.shifts_date:
            if (date_time_obj.date() >= st_time.date() and date_time_obj.date() <= end_time.date()):
                dates_list.append(date_time_obj)
        if float(patient.id_p) == 384830772.0:
            print ("surgeon: ",patient.id_p, " : ",dates_list)
        return dates_list
                



class Coordinator:
    def __init__(self,allocation_rooms  ,dates):
        Thread.__init__(self)
        self.allocation_rooms = allocation_rooms
        self.requests = []
        self.dict_src = Scheduling.Communication.dict_src
        self.requests = initializing.get_requests_agent("1.1")
        
        # self.request_by_date ={}
        # for date in dates:
        #     self.request_by_date[date] = []
        
     
        
    # def run(self):
    #     print("here i am")
    #     # with self.cvM:
    #     #     print("putting msg")
    #     #     #msg = Messages.Msg(0,1,"hello from CA")
    #     #    #test.Main.global_queue.put("msg")
    #     #     Head_ward.queue_how.put("hello from ca")
    #     #     print("putting msg")
    #     #     self.cvM.notifyAll()
    #     Scheduling.Communication.global_queue.put("hello from CA")
    #     print(Scheduling.Communication.global_queue.empty())
    #     print("sending messages ca")
    
    def restrickDomain(self):
        req_send_AAs = []
        
        for unit in self.requests:
            for key in self.requests[unit]:
                req = self.requests[unit][key]
                if float(req.id_p) == 384830772.0:
                    print("from hereeeeeeeee" ,req.surgeon)
                if len(str(req.surgeon))>0 and  req.surgeon!=None :
                    req_send_AAs.append(req)
                if (len(str(req.deadline))>0 and req.deadline!=None):
                    req_send_AAs.append(req)
                if (len(str(req.from_date))>0 and req.from_date!=None):
                    req_send_AAs.append(req)
        msg = Messages.Msg(0,self.dict_src["CA"],self.dict_src["HoW"],req_send_AAs )
        return msg
        
        
        
    
    def producer(self, dict_HOW, dict_ORM):
        for unit in self.requests:
            for key in self.requests[unit]:
                req = self.requests[unit][key]
                if (req.id_p in dict_HOW):
                    set_how = dict_HOW[req.id_p]
                    set_orm = dict_ORM[req.id_p]
                    inte = list(set(set_orm).intersection(set_how))
                else:
                    inte = dict_ORM[float(req.id_p)]
                    #inte = check_pre_operation_available(inte) checking if there is available date for pre operation
                #print(req.id_p," : dates: ", inte)
                #print ("CA: ",req.id_p, " : ",inte, "HOW - ", dict_HOW[req.id_p], " ORM: ",dict_ORM[req.id_p])

                # if float(req.id_p) == 384830772.0:
                #     print ("CA: ",req.id_p, " : ",inte, "HOW - ", dict_HOW[req.id_p], " ORM: ",dict_ORM[req.id_p])
                req.add_to_domain(inte)
        
        


    def get_src(self):
        return Messages.Src.CA
    
    def convert_to_dict(self, requests):
       set_p = []
       for req in requests:
           set_p.append(req.dict_p())
           
       return set_p
    
    
 #sending options for room and date, return info msg to send   
    def set_option_for_room(self,date,room):
        patients_specific = []                            #list of relevant patients
        options = []                                      # combinations
        num_of_option = 3                                 # num of combination
        num_of_request = Rooms.get_num_by_unit(room.unit1, room.get_len_patients()) #number of request at ine combination
        patient_by_unit = self.requests[room.unit1]
        for key in patient_by_unit:
            p = patient_by_unit[key]
            #if there is intersection between the date of rooms to request
            #print(p.domain)
            date_intersection =(next((date_p for date_p in p.domain if datetime.datetime.strptime(date_p, "%Y-%m-%d %H:%M:%S") == date), None)) 
            if date_intersection !=None:
                patients_specific.append(p)
        #sorted by pricing of the coordinator
        patients_specific = sorted(patients_specific, key= self.pricing)
        for num_combonation in range(0,num_of_option):
            list_id = [p.id_p for p in patients_specific[:num_of_request]]
            # print("Option mum: ", num_combonation)
            # print(list_id)
            options.append(Option(date.strftime("%Y-%m-%d %H:%M:%S"),Rooms.convert_code_name(room.unit1),patients_specific[:num_of_request]))
            
            patients_specific = patients_specific[num_of_request:]
        
        
        info = {}
        info[0] = options
        # for opt in options:
        #     print(opt.date)
        #     for re in opt.requests:
        #         print(re.id_p, re)
        list_id = [p.id_p for p in patients_specific]
        #print(list_id)
        info[1] = list_id
        
        return info
    
    def remove_patients_list(self,pt_list, unit):
        for pt in pt_list:
            del self.requests[unit][float(pt.id_p)]
        
    def  set_pricing_new_opt(self, options, how_pricing):
        pricing = {}
        price = 0
        i = 0
        
        for opt in options:
            unit = Rooms.convert_name_code(opt.unit)
            for req in opt.requests:
                #req = self.requests[unit][float(req_id)]
                price += self.pricing(req)
            #print(i, ". price CA: ",price,"  price HOW: ", how_pricing[i])
            pricing[i] = price + how_pricing[i]
            price = 0
            i += 1
        return pricing
        
        
    def inter_pricing(self, options, how_pricing):
        how_pricing = {int(k):v for k,v in how_pricing.items()}
        pricing = self.set_pricing_new_opt(options[0], how_pricing)
        key = min(pricing, key=pricing.get)
        win_opt = options[0][key]
        date = datetime.datetime.strptime(win_opt.date, "%Y-%m-%d %H:%M:%S")
        rooms = self.allocation_rooms[date]
        for room in rooms:
            if room.unit1 == Rooms.convert_name_code(win_opt.unit):
                #list_pt = self.get_pt_by_id(win_opt.requests,room.unit1)
                room.add_patients(win_opt.requests)
                self.remove_patients_list(win_opt.requests,room.unit1 )
                break
    
#setting date of pre-operation after scheduling in room allocation
    def set_pre_op_date(self):
        pre_operations_dates = Rooms.create_pre_operation_times()
        for date in self.allocation_rooms:
            for room in self.allocation_rooms[date]:
                for p in room.patients:
                    date_pre_opt = Rooms.get_available_dates(pre_operations_dates,date,p.unit)
                    pre_operations_dates[date_pre_opt].add_patient(p)
                    p.set_pre_op_date(date_pre_opt)
            
    
#print room allocation and send partial scheduling to database
    def print_room_allocations(self, send_data_base = False):
        self.set_pre_op_date()
        print("ALLOCATION ROOM: \n")
        for date in self.allocation_rooms:
            for room in self.allocation_rooms[date]:
                #if room.unit1 == Rooms.convert_name_code('Bariatric'):
                    print("date: ", date)
                    for p in room.patients:
                        print(p.id_p, " Scheduled: ", p.is_scheduled(), " ",p.scheduling_date," req id: ", p.id_req, " ", p.pre_operation_date )
        #initializing.send_to_file(self.allocation_rooms)
        flag_update_status = True
        if send_data_base:
            initializing.update_pre_op_dates_requests_surgery(self.allocation_rooms)
            initializing.send_to_partial_scheduling_data_base(self.allocation_rooms, flag_update_status)
                
 
#getting pt list by id list and unit        
    def get_pt_by_id(self, list_id, unit):
        req_unit = self.requests[unit]
        pt_list = []
        for id_p in list_id:
            pt_list.append(req_unit[float(id_p)])
        return pt_list
    
    def pricing(self, request):
        price_d = 0
        price_c = 0
        price_p = 0
        #print("here in pricing CA: ", request.deadline, "  len:",len(str(request.deadline)))
        if (len(str(request.deadline))>0 and request.deadline!=None ):
            delta = request.deadline -datetime.datetime(2021, 4, 1)
            price_d += delta.days*0.5
        elif request.urgency == 3:
            price_d = 0
        else:
            price_d= 15
            
        if (request.num_cancelation == 0):
            price_c += 5
        elif (request.num_cancelation == 1):
            price_c +=2
        
        #print("here in pre op != None",request.pre_operation_date!= None)
        
        #if (len(str(request.pre_operation_date))!=0):
        if (request.pre_operation_date!= None):
            expired = request.pre_operation_date + datetime.timedelta(days=60)
            delta = expired -datetime.datetime(2021, 4, 1)
            price_p += delta.days*0.5
        else:
            price_p = 15
        
        #print(request.id_p, "CA : ",0.4*price_d + 0.25*price_c + 0.35*price_p)
        return (0.4*price_d + 0.25*price_c + 0.35*price_p)
            
#    def set_price_on_request()
        
        





        
def writing_excel_file(result):
    # Create the workbook and sheet for Excel
    workbook = openpyxl.Workbook()
    sheet = workbook.active
    
    # openpyxl does things based on 1 instead of 0
    row = 1
    for key,values in result.items():
        # Put the key in the first column for each key in the dictionary
        sheet.cell(row=row, column=1, value=key)
        column = 2
        for element in values:
            # Put the element in each adjacent column for each element in the tuple
            sheet.cell(row=row, column=column, value=element.__str__())
            column += 1
        row += 1
    workbook.save(filename="D:\master\Research\Model\my_workbook.xlsx")     

                    
                
                
                
   
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                