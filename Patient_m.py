# -*- coding: utf-8 -*-
"""
Created on Mon Sep  7 18:54:45 2020

@author: dell
"""

import Rooms
import xlrd 
import datetime
from queue import Queue
import json


class Patient:
    def __init__(self,id_p,arrival, deadline,from_date, surgeon, urgency,complexity, surgery_t,unit,scheduling_date,num_cancelation=0, pre_operation_date =None, req_id = -1):
        self.id_p =float(id_p)
        self.arrival = arrival
        self.deadline = deadline
        self.from_date = from_date
        self.surgeon = surgeon
        self.urgency = urgency
        self.complexity = int(complexity)
        self.surgery_t = surgery_t
        self.num_cancelation = num_cancelation
        self.id_req = req_id
        self.specific_domain = []
        self.head_ward_queue = Queue()
        self.management_queue = Queue()
        self.unit = unit
        self.pre_operation_date = pre_operation_date
        self.domain = []
        self.domain_surgeons = []
        self.scheduling_date = scheduling_date
 

    def __str__(self):
        return str(self.id_p)+" arrival: "+str(self.arrival)+" deadline: "+str(self.deadline)+ " surgeon :"+str(self.surgeon)+" urgency: "+str(self.urgency)+" comlexity: "+str(self.complexity)+ " surgery type: "+str(self.surgery_t)+" unit: "+str(self.unit)+" num of cancelation: "+str(self.num_cancelation)+" pre op date: "+str(self.pre_operation_date)

    def __lt__(self, other):

        return self.arrival < other.arrival
     
    def add_surgeon_domain(self, surgeon):
        self.domain_surgeons.append(surgeon)
        
    def head_ward_queue_add(self, dates):
        for date in dates:
            self.head_ward_queue.put(date)
    
    def mangement_queue_add(self, dates):
        for date in dates:
            self.management_queue.put(date)
    
    def add_to_domain(self, dates):
        self.domain.extend(dates)
    
    def set_id_req(self, id_req):
        self.id_req = id_req
    
    def set_scheduling_date(self, date):
        self.scheduling_date = date
    def is_scheduled(self):
        return self.scheduling_date !=None
    
    def set_pre_op_date(self, pre_op_date):
        self.pre_operation_date = pre_op_date
        
    def dict_p(self):
        di_ct = {}
        di_ct["id"] = self.id_p
        di_ct["arrival"] = self.arrival.strftime("%Y-%m-%d %H:%M:%S")
        if len(str(self.deadline)) != 0:
            di_ct["deadline"] = self.deadline.strftime("%Y-%m-%d %H:%M:%S")
        if len(str(self.from_date)) != 0:
            di_ct["from_date"] = self.from_date.strftime("%Y-%m-%d %H:%M:%S")
        if len(str(self.pre_operation_date)) != 0:   
            di_ct["pre_operation_date"] = self.pre_operation_date.strftime("%Y-%m-%d %H:%M:%S")
        di_ct["surgeon"] = self.surgeon
        di_ct["urgency"] = self.urgency
        di_ct["complexity"] = self.complexity
        di_ct["surgery_t"] = self.surgery_t
        di_ct["num_cancelation"] = self.num_cancelation
        di_ct["unit"] = Rooms.convert_code_name( self.unit)
        return di_ct
 
#convert unit to num unit by data base
def convert_name_code(code_p, type_op):
    code = Rooms.convert_name_code(code_p)
    if code == Rooms.Unit.Bariatric:   
        return 31
    if code == Rooms.Unit.General:
        if type_op =="gall bladder":
            return 19
        if type_op == "hernia":
            return 20
        if type_op == "Anal fissure":
            return 12
        return 32
    if code == Rooms.Unit.Proctology:
        if type_op == "piles":
            return 22
        return 33
    if code == Rooms.Unit.Hospitalization_Day:
        if type_op =="biopsy":
            return 14

        if type_op == "hernia":
            return 21
        return 34
    if code == Rooms.Unit.Colon:
        if type_op == "cancer":
            return 15
        return 35
    if type_op == "cyst":
            return 17
    return 36


#convert unit to num unit by data base
def convert_code_name(code):
    if code == 31:      
        return Rooms.Unit.Bariatric,"bariatric"
    if code == 19:
        return Rooms.Unit.General,"gall bladder"
    if code == 20:
        return Rooms.Unit.General,"hernia"
    if code == 12:
        return Rooms.Unit.General ,"Anal fissure"
    if code == 32:
        return Rooms.Unit.General, "General"
    if code == 22:
        return Rooms.Unit.Proctology, "piles"
    if code == 33:
        return Rooms.Unit.Proctology, "Proctology"
    if code == 14:
        return Rooms.Unit.Hospitalization_Day, "biopsy"
    if code == 21:
        return Rooms.Unit.Hospitalization_Day, "hernia"
    if code == 34:
        return Rooms.Unit.Hospitalization_Day, "Hospitalization_Day"
    if code == 15:
        return Rooms.Unit.Colon, "cancer"
    if code == 35:
        return Rooms.Unit.Colon, "colon"
    if code == 17:
         return Rooms.Unit.Activity_Room  ,"cyst"
    return Rooms.Unit.Activity_Room, "Activity_Room"


def create_dict_patient(path):

    patients = {}
    patients[Rooms.Unit.Bariatric] = {}
    patients[Rooms.Unit.Activity_Room] = {}
    patients[Rooms.Unit.Proctology] = {}
    patients[Rooms.Unit.Colon] = {}
    patients[Rooms.Unit.Hospitalization_Day] = {}
    patients[Rooms.Unit.General] = {}
    loc = (path) 
  
    wb = xlrd.open_workbook(loc) 
    sheet = wb.sheet_by_index(0)
  
    for i in range(1,sheet.nrows):
        id_p = sheet.cell_value(i, 7)
        a = sheet.cell_value(i, 6)
        d = sheet.cell_value(i, 5)
        f = sheet.cell_value(i, 10)
        pre_op = sheet.cell_value(i, 9)
        arrival = datetime.datetime(*xlrd.xldate_as_tuple(a, wb.datemode))
        if len(str(d))>0:
            d = datetime.datetime(*xlrd.xldate_as_tuple(d, wb.datemode))
        if len(str(f))>0:
            f = datetime.datetime(*xlrd.xldate_as_tuple(f, wb.datemode))
        if len(str(pre_op))>0:
            pre_op = datetime.datetime(*xlrd.xldate_as_tuple(pre_op, wb.datemode))
        else:
            pre_op = None
        surgeon = sheet.cell_value(i, 4)
        urgency = sheet.cell_value(i, 3)
        complexity = sheet.cell_value(i, 2)
        surgery_t = sheet.cell_value(i, 1)
        unit = sheet.cell_value(i, 0)
        num_cancellation = sheet.cell_value(i, 11)
        scheduling_date = sheet.cell_value(i, 12)
        if len(str(scheduling_date))>0:
            scheduling_date = datetime.datetime(*xlrd.xldate_as_tuple(scheduling_date, wb.datemode))
        else:
            scheduling_date = None
        #if Rooms.convert_name_code(unit) not in patients:
        #    patients[Rooms.convert_name_code(unit)] = []
       # patients[Rooms.convert_name_code(unit)].append(Patient(id_p,arrival,d,f, surgeon, urgency, complexity, surgery_t, unit,scheduling_date,num_cancellation,pre_op))
    #set_patients_to_data_base(patients, status)
        patients[Rooms.convert_name_code(unit)][id_p] = ( Patient(id_p,arrival,d,f, surgeon, urgency, complexity, surgery_t, unit,num_cancellation,pre_op))
    return patients

