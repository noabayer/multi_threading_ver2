# -*- coding: utf-8 -*-
"""
Created on Mon Sep  7 18:57:22 2020

@author: dell
"""

from enum import Enum
import datetime


#class of room that defone surgery room + room of pre-operation

class Unit(Enum):
     Bariatric = 1
     General = 2
     Proctology = 3
     Hospitalization_Day = 4
     Colon = 5
     Activity_Room = 6

def convert_name_code(type_s):
    if type_s == 'bariatric':
        return Unit.Bariatric
    if type_s == 'general':
        return Unit.General
    if type_s == 'proctology':
        return Unit.Proctology
    if type_s == 'Hospitalization Day':
        return Unit.Hospitalization_Day
    if type_s == 'Colon':
        return Unit.Colon
    return Unit.Activity_Room


def create_rooms_allocation():
    rooms = {}
    sun1 = datetime.datetime(2020,7,5)
    sun2 = datetime.datetime(2020,7,12)
    sun3 = datetime.datetime(2020,7,19)
    sun4 = datetime.datetime(2020,7,26)
    tue1 = datetime.datetime(2020,7,7)
    tue2 = datetime.datetime(2020,7,14)
    tue3 = datetime.datetime(2020,7,21)
    tue4 = datetime.datetime(2020,7,28)
    wed1 = datetime.datetime(2020,7,8)
    wed2 = datetime.datetime(2020,7,15)
    wed3 = datetime.datetime(2020,7,22)
    wed4 = datetime.datetime(2020,7,29)
    thu1 = datetime.datetime(2020,7,9)
    thu2 = datetime.datetime(2020,7,16)
    thu3 = datetime.datetime(2020,7,23)
    thu4 = datetime.datetime(2020,7,30)
    rooms[sun1] = (Room_m(1,Unit.Bariatric),Room_m(2,Unit.General))
    rooms[sun2] = (Room_m(1,Unit.Bariatric),Rooms(2,Unit.General))
    rooms[sun3] = (Room_m(1,Unit.Bariatric),Rooms(2,Unit.General))
    rooms[sun4] = (Room_m(1,Unit.Bariatric),Rooms(2,Unit.General),Rooms(3,Unit.Activity_Room))
    rooms[tue1] = (Room_m(1,Unit.Colon),Rooms(2,Unit.Hospitalization_Day))
    rooms[tue2] = (Room_m(1,Unit.Colon),Rooms(2,Unit.Hospitalization_Day))
    rooms[tue3] = (Room_m(1, Unit.Colon),Rooms(2,Unit.Hospitalization_Day))
    rooms[tue4] = (Rooms(1,Unit.Colon),Rooms(2,Unit.Hospitalization_Day))
    rooms[wed1] = (Rooms(1,Unit.General),Rooms(2,Unit.Colon))
    rooms[wed2] = (Rooms(1,Unit.General),Rooms(2,Unit.Colon))
    rooms[wed3] = (Rooms(1,Unit.General),Rooms(2,Unit.Colon))
    rooms[wed4] = (Rooms(1,Unit.General),Rooms(2,Unit.Colon))
    rooms[thu1] = (Rooms(1,Unit.General),Rooms(2,Unit.Proctology))
    rooms[thu2] = (Rooms(1,Unit.General),Rooms(2,Unit.Proctology))
    rooms[thu3] = (Rooms(1,Unit.General),Rooms(2,Unit.Proctology))
    rooms[thu4] = (Rooms(1,Unit.General),Rooms(2,Unit.Proctology))
    return rooms, rooms.keys