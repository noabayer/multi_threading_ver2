# -*- coding: utf-8 -*-
"""
Created on Sat Jul  4 20:10:49 2020

@author: dell
"""
from enum import Enum
import datetime
import collections
import initializing


#class of room that defone surgery room + room of pre-operation


class Unit(Enum):
     Bariatric = 1
     General = 2
     Proctology = 3
     Hospitalization_Day = 4
     Colon = 5
     Activity_Room = 6

def convert_name_code(type_s):
    if type_s == 'bariatric' or type_s == 'Bariatric':
        return Unit.Bariatric
    if type_s == 'general' or type_s == 'General':
        return Unit.General
    if type_s == 'proctology' or  type_s == 'Proctology' :
        return Unit.Proctology
    if type_s == 'Hospitalization Day':
        return Unit.Hospitalization_Day
    if type_s == 'Colon' or type_s == 'colon':
        return Unit.Colon
    return Unit.Activity_Room

def convert_code_name(code):
    if code == Unit.Bariatric:
        return 'Bariatric'
    if code == Unit.General:
        return 'General'
    if code == Unit.Proctology:
        return 'Proctology'
    if code == Unit.Hospitalization_Day:
        return 'Hospitalization Day'
    if code == Unit.Colon:
        return 'Colon'
    return 'Activity Room'

    
     
     
class Rooms():
     rooms_allocations = {}
     def __init__(self,num, unit1):
         self.num = num
         self.unit1 = unit1
         self.unit_str = convert_code_name(self.unit1)
         self.patients = []
         self.options = {}
         
     def add_patient(self, patient):
         self.patients.append(patient)
         
     def add_patients(self,list_patients):
        for p in list_patients:
            self.patients.append(p)

     def add_option(self, option, price):
        self.options[price.get_average()] = (price, option)
        self.options = collections.OrderedDict(sorted(self.options.items(),reverse=True))



     def remove_patient(self, patient):
        self.patients.remove(patient)
         
     def get_len_patients(self):
         return len(self.patients)
     
     def __str__(self):
        return str(self.unit1)
 
       
  
def create_rooms_allocation(update_data_base = False):
    # rooms = {}
    # sun1 = datetime.datetime(2020,7,5)
    # sun2 = datetime.datetime(2020,7,12)
    # sun3 = datetime.datetime(2020,7,19)
    # sun4 = datetime.datetime(2020,7,26)
    # tue1 = datetime.datetime(2020,7,7)
    # tue2 = datetime.datetime(2020,7,14)
    # tue3 = datetime.datetime(2020,7,21)
    # tue4 = datetime.datetime(2020,7,28)
    # wed1 = datetime.datetime(2020,7,8)
    # wed2 = datetime.datetime(2020,7,15)
    # wed3 = datetime.datetime(2020,7,22)
    # wed4 = datetime.datetime(2020,7,29)
    # thu1 = datetime.datetime(2020,7,9)
    # thu2 = datetime.datetime(2020,7,16)
    # thu3 = datetime.datetime(2020,7,23)
    # thu4 = datetime.datetime(2020,7,30)
    # rooms[sun1] = (Rooms(1,Unit.Bariatric),Rooms(2,Unit.General))
    # rooms[sun2] = (Rooms(1,Unit.Bariatric),Rooms(2,Unit.General))
    # rooms[sun3] = (Rooms(1,Unit.Bariatric),Rooms(2,Unit.General))
    # rooms[sun4] = (Rooms(1,Unit.Bariatric),Rooms(2,Unit.General),Rooms(3,Unit.Activity_Room))
    # rooms[tue1] = (Rooms(1,Unit.Colon),Rooms(2,Unit.Hospitalization_Day))
    # rooms[tue2] = (Rooms(1,Unit.Colon),Rooms(2,Unit.Hospitalization_Day))
    # rooms[tue3] = (Rooms(1, Unit.Colon),Rooms(2,Unit.Hospitalization_Day))
    # rooms[tue4] = (Rooms(1,Unit.Colon),Rooms(2,Unit.Hospitalization_Day))
    # rooms[wed1] = (Rooms(1,Unit.General),Rooms(2,Unit.Colon))
    # rooms[wed2] = (Rooms(1,Unit.General),Rooms(2,Unit.Colon))
    # rooms[wed3] = (Rooms(1,Unit.General),Rooms(2,Unit.Colon))
    # rooms[wed4] = (Rooms(1,Unit.General),Rooms(2,Unit.Colon))
    # rooms[thu1] = (Rooms(1,Unit.General),Rooms(2,Unit.Proctology))
    # rooms[thu2] = (Rooms(1,Unit.General),Rooms(2,Unit.Proctology))
    # rooms[thu3] = (Rooms(1,Unit.General),Rooms(2,Unit.Proctology))
    # rooms[thu4] = (Rooms(1,Unit.General),Rooms(2,Unit.Proctology))
    # return rooms, rooms.keys
    if not Rooms.rooms_allocations:
        if update_data_base :
            initializing.initialize_data_base("D:\master\Research\multiple_client\data\partial_scheduled.xlsx", "D:\master\Research\multiple_client\data\data_bariatric.xlsx")
        rooms_allocations, dates = initializing.getting_room_allocation_for_surgical();
        patients_scheduled = initializing.get_requests("1.3")
        Rooms.rooms_allocations = initializing.fill_roomallocation(patients_scheduled, rooms_allocations)
        if update_data_base:
            initializing.send_scheduling_data_base(Rooms.rooms_allocations)
    return Rooms.rooms_allocations, Rooms.rooms_allocations.keys()


def create_rooms_allocation_old_ver():
    rooms = {}
    sun1 = datetime.datetime(2020,7,5)
    sun2 = datetime.datetime(2020,7,12)
    sun3 = datetime.datetime(2020,7,19)
    sun4 = datetime.datetime(2020,7,26)
    tue1 = datetime.datetime(2020,7,7)
    tue2 = datetime.datetime(2020,7,14)
    tue3 = datetime.datetime(2020,7,21)
    tue4 = datetime.datetime(2020,7,28)
    wed1 = datetime.datetime(2020,7,8)
    wed2 = datetime.datetime(2020,7,15)
    wed3 = datetime.datetime(2020,7,22)
    wed4 = datetime.datetime(2020,7,29)
    thu1 = datetime.datetime(2020,7,9)
    thu2 = datetime.datetime(2020,7,16)
    thu3 = datetime.datetime(2020,7,23)
    thu4 = datetime.datetime(2020,7,30)
    rooms[sun1] = (Rooms(1,Unit.Bariatric),Rooms(2,Unit.General))
    rooms[sun2] = (Rooms(1,Unit.Bariatric),Rooms(2,Unit.General))
    rooms[sun3] = (Rooms(1,Unit.Bariatric),Rooms(2,Unit.General))
    rooms[sun4] = (Rooms(1,Unit.Bariatric),Rooms(2,Unit.General),Rooms(3,Unit.Activity_Room))
    rooms[tue1] = (Rooms(1,Unit.Colon),Rooms(2,Unit.Hospitalization_Day))
    rooms[tue2] = (Rooms(1,Unit.Colon),Rooms(2,Unit.Hospitalization_Day))
    rooms[tue3] = (Rooms(1, Unit.Colon),Rooms(2,Unit.Hospitalization_Day))
    rooms[tue4] = (Rooms(1,Unit.Colon),Rooms(2,Unit.Hospitalization_Day))
    rooms[wed1] = (Rooms(1,Unit.General),Rooms(2,Unit.Colon))
    rooms[wed2] = (Rooms(1,Unit.General),Rooms(2,Unit.Colon))
    rooms[wed3] = (Rooms(1,Unit.General),Rooms(2,Unit.Colon))
    rooms[wed4] = (Rooms(1,Unit.General),Rooms(2,Unit.Colon))
    rooms[thu1] = (Rooms(1,Unit.General),Rooms(2,Unit.Proctology))
    rooms[thu2] = (Rooms(1,Unit.General),Rooms(2,Unit.Proctology))
    rooms[thu3] = (Rooms(1,Unit.General),Rooms(2,Unit.Proctology))
    rooms[thu4] = (Rooms(1,Unit.General),Rooms(2,Unit.Proctology))
    return rooms

class room_pre_operation():
    def __init__(self,len_p, unit):
        self.len_p = len_p
        self.unit = unit
        self.patients = []
    def add_patient(self, patient):
        self.patients.append(patient)
        self.len_p -= 1

#allocation rooms for pre-operation 
def create_pre_operation_times():
    rooms_pre_operation= {}
    rooms_pre_operation[datetime.datetime(2021,3,11)] = room_pre_operation(5,Unit.General)
    rooms_pre_operation[datetime.datetime(2021,3,18)] = room_pre_operation(7,Unit.General)
    rooms_pre_operation[datetime.datetime(2021,3,25)] = room_pre_operation(7,Unit.General)
    rooms_pre_operation[datetime.datetime(2021,3,30)] = room_pre_operation(5,Unit.Proctology)
    
    rooms_pre_operation[datetime.datetime(2021,4,1)] = room_pre_operation(25,Unit.General)
    rooms_pre_operation[datetime.datetime(2021,4,8)] = room_pre_operation(27,Unit.General)
    rooms_pre_operation[datetime.datetime(2021,4,15)] = room_pre_operation(25,Unit.General)
    rooms_pre_operation[datetime.datetime(2021,4,22)] = room_pre_operation(25,Unit.General)
    rooms_pre_operation[datetime.datetime(2021,4,29)] = room_pre_operation(25,Unit.General)
    
    rooms_pre_operation[datetime.datetime(2021,4,6)] = room_pre_operation(15,Unit.Proctology)
    rooms_pre_operation[datetime.datetime(2021,4,13)] = room_pre_operation(15,Unit.Proctology)
    return rooms_pre_operation

#get available dates for pre-operation
def get_available_dates(pre_operation, date_patient, unit):
    dates = pre_operation.keys()
    list_p_date = []
    for date in dates:
        if date < date_patient and  pre_operation[date].len_p !=0:
            #if date is of procto and also operation is procto or both of them is not procto
            if unit == pre_operation[date].unit ==Unit.Proctology or (unit != Unit.Proctology and pre_operation[date].unit !=Unit.Proctology):
                list_p_date.append(date)

    list_p_date = sorted(list_p_date, reverse=True)
    return list_p_date[0]
 

#getting num by Unit
def get_num_by_unit(unit, scheduled_p =0, precent = 0):
    if unit == Unit.Bariatric or unit == Unit.Hospitalization_Day or unit == Unit.Activity_Room:
        return 4 - scheduled_p
    if unit == Unit.General or unit == Unit.Colon:
        return 3 - scheduled_p
    return 5 - scheduled_p
           

# if __name__ == '__main__':
#     create_rooms_allocation(True)

    
    
    
    
    
    
    
    
    
    
    