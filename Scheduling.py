# -*- coding: utf-8 -*-
"""
Created on Tue Sep 15 16:43:39 2020

@author: dell
"""

from threading import Thread 
import Classes
import Rooms
import Patient_m
from queue import Queue
import openpyxl
import time
import Messages
from threading import Lock 
import requests
import initializing

clients_lock = Lock()


class agent_CA(Thread):
    queue_ca = Queue()
    def __init__(self):
        Thread.__init__(self)
        allocation_rooms, dates = Rooms.create_rooms_allocation()
        self.ca = Classes.Coordinator(allocation_rooms, dates)
        self.pairs_dates = self.making_pairs(dates, allocation_rooms)
    
    def making_pairs(self,date, allocation_rooms):
        dict_pairs = {}
        counter = 0
        for date in allocation_rooms:
            for room in allocation_rooms[date]:
            #     if room.unit1 == Rooms.Unit.Bariatric:
                   dict_pairs[counter] = (date, room)
                   counter += 1
        return dict_pairs
    
  
    def send_msg(self,msg):
        Communication.global_queue.put(msg)
    

    def run(self):  
        flag_HOW = False
        flag_ORM = False
        counter = 0
        self.send_msg(self.ca.restrickDomain())
        self.send_msg(Messages.Msg(0,Communication.dict_src["CA"],Communication.dict_src["ORM"],self.ca.requests))
        while True:
            msg = self.queue_ca.get()
            if msg.code == 0 and msg.src== Communication.dict_src["HoW"]:
                flag_HOW = True
                dict_HOW = msg.info
                clients_lock.acquire()
                print("got date domain from HOw")
                clients_lock.release()
            if msg.code == 0 and msg.src== Communication.dict_src["ORM"]:
                flag_ORM = True
                dict_ORM = msg.info
                clients_lock.acquire()
                print("got date domain from ORM")
                clients_lock.release()
            if flag_HOW and flag_ORM:
                print("Making intersection! ")
                dict_ORM = {float(k):v for k,v in dict_ORM.items()}
                dict_HOW = {float(k):v for k,v in dict_HOW.items()}
                self.ca.producer(dict_HOW,dict_ORM)
                flag_ORM = False
                flag_HOW = False
                info = self.ca.set_option_for_room(self.pairs_dates[counter][0], self.pairs_dates[counter][1])
                self.send_msg(Messages.Msg(1,Communication.dict_src["CA"],Communication.dict_src["HoW"],info))
                counter += 1
            if msg.code == 1:
                print("PRICING : ")
                how_pricing = msg.info
                self.ca.inter_pricing(info,how_pricing)        
                if (counter < len(self.pairs_dates)):
                    #print("sending next options ")
                    info = self.ca.set_option_for_room(self.pairs_dates[counter][0], self.pairs_dates[counter][1])
                    self.send_msg(Messages.Msg(1,Communication.dict_src["CA"],Communication.dict_src["HoW"],info))
                    counter += 1
                else:
                    #printing room allocation and according to flag(False/True) updating data base
                    self.ca.print_room_allocations(True)
                    self.send_msg(Messages.Msg(8,Communication.dict_src["CA"],Communication.dict_src["HoW"],""))
                    self.send_msg(Messages.Msg(9,Communication.dict_src["CA"],-1,""))
                    break


class agent_HoW(Thread):
    queue_HoW = Queue()
    def __init__(self,allocation_rooms = None ,dates= None):
        Thread.__init__(self)
        self.how = Classes.Head_ward(2)
  
    def send_msg(self, msg):
        Communication.global_queue.put(msg)
        
   
    def run(self):  
        while True:
            msg = self.queue_HoW.get()
            clients_lock.acquire()
            #print("HOW get msg:", msg.code)
            #print("handle task")
            clients_lock.release()
            if msg.code == 0:
                msg_send = self.how.producer(msg.info)
                self.send_msg(msg_send)
                self.queue_HoW.task_done()
            if msg.code == 1:
                #print("getting option ! - HOW")
                pricing = self.how.response_with_pricing(msg.info[0], msg.info[1])
                self.send_msg(Messages.Msg(1,Communication.dict_src["HoW"],Communication.dict_src["CA"],pricing))
                #print("sending pricing")
            if msg.code == 8:
                self.send_msg(Messages.Msg(9,Communication.dict_src["HoW"],-1,""))
                break
                
   


class agent_ORM(Thread):
    queue_ORM = Queue()
    def __init__(self,num_ward):
        Thread.__init__(self)
        allocation_rooms, __ = Rooms.create_rooms_allocation()
        self.mangement = Classes.Management(num_ward,allocation_rooms)
        print("here in orm init")
  
    def send_msg(self, msg):
        Communication.global_queue.put(msg)

    def run(self):  
        print("here in orm)")
        while True:
            msg = self.queue_ORM.get()
            clients_lock.acquire()
            print("ORM get msg:", msg.code)
            print("handle task")
            clients_lock.release()
            if msg.code == 0:
                msg_send = self.mangement.producer(msg.info)
                self.send_msg(msg_send)
                self.queue_ORM.task_done()
            self.send_msg(Messages.Msg(9,Communication.dict_src["ORM"],-1,""))
            break

class Communication(Thread): 
    global_queue = Queue()
    dict_src = {"CA":0, "HoW":1, "ORM":3}
    def __init__(self):
        Thread.__init__(self)
    
    
    def run(self):
        num_threads = 3
        non_active_tr = 0
        #print("here in queue ")
        while num_threads != non_active_tr:
            msg = self.global_queue.get()
            clients_lock.acquire()
            #print("sending msg: from:", msg.src,"to: ", msg.dst)
            clients_lock.release()
            if msg.code == 9:
                non_active_tr +=1 
            if msg.dst == self.dict_src["CA"]:
                agent_CA.queue_ca.put(msg)
            if msg.dst == self.dict_src["HoW"]:
                agent_HoW.queue_HoW.put(msg)
            if msg.dst == self.dict_src["ORM"]:
                agent_ORM.queue_ORM.put(msg)
            self.global_queue.task_done()







if __name__ == '__main__':

   
    t = Communication()
    t1 = agent_CA()
    t2 = agent_HoW(2)
    t3 = agent_ORM(2)
   
    t1.start()
    t2.start()
    t3.start()

    t.start()
    
    t1.join()
    t2.join()
    t3.join()
    t.join()
    print("DONE")

 


        
        
        
        
        
        
        
        