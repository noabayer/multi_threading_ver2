# -*- coding: utf-8 -*-
"""
Created on Sun Feb 28 13:29:22 2021

@author: dell
"""


import Patient_m
import requests
import datetime

def get_removed_requests():
    patients = []
    URL = "https://www.api.orm-bgu-soroka.com/surgeries_surgical_noas/get_all_removed_surgeries"
    r = requests.get(url = URL) 
    removes_requests = r.json()
    # print(data)
    for i in range(len(removes_requests)):
        request_id = removes_requests[i]['request_id']
        URL = "https://www.api.orm-bgu-soroka.com/surgery_requests/get_by_request_id"
        PARAMS = {"request_id": str(request_id)}
        req = requests.get(url = URL, json =PARAMS)
        data =req.json()
        pre_op_date =None
        schedule_date = None
        schedule_from = None
        schedule_deadline = None
        request_id = data[i]['request_id']
        unit, surgery_t = Patient_m.convert_code_name(data[i]['surgery_type_fk'])
        arrival = datetime.datetime.strptime(data[i]['entrance_date'], '%Y-%m-%d')
        if data[i]['pre_op_date'] !=None:
            pre_op_date = datetime.datetime.strptime(data[i]['pre_op_date'], '%Y-%m-%d')
        if data[i]['schedule_date'] != None:
            schedule_date = datetime.datetime.strptime(data[i]['schedule_date'], '%Y-%m-%d')
        if data[i]['schedule_from'] !=None:
            schedule_from = datetime.datetime.strptime(data[i]['schedule_from'], '%Y-%m-%d')
        if data[i]['schedule_deadline']!=None:
            schedule_deadline = datetime.datetime.strptime(data[i]['schedule_deadline'], '%Y-%m-%d')
        
        patients.append(Patient_m.Patient(data[i]['patient_id'],arrival,schedule_deadline,schedule_from, data[i]['specific_senior'], data[i]['urgency'], data[i]['complexity'], surgery_t, unit,schedule_date,data[i]['cancellations'],pre_op_date, request_id))
    return patients
    

def handle_removed_requests(patients):
    URL_notifications = "https://www.api.orm-bgu-soroka.com/surgical_ward_notifications/add_notification"
    URL_remove = "https://www.api.orm-bgu-soroka.com/surgeries_surgical_noas/delete_surgery"
    description_c = "Number of cancellation is high - "
    description_d_pre = "Pre-operation date is about to expire - "
    description_removed = "The request removes succesfully "
    for p in patients:
       # difference = p.scheduling_date - p.pre_operation_date
        #print(difference.days)
        if p.num_cancelation >1 :
            print("here in cancellation number")
            requests.post(URL_remove, json={'request_id': p.id_req})
            requests.post(URL_notifications, json={ 'request_id': p.id_req, 'description': description_c + str(p.num_cancelation), 'answer':"n/a"})
        #elif difference.days < 60:
         #   requests.post(URL_notifications, json={ 'request_id': p.id_req, 'description': description_d_pre + p.pre_operation_date.strftime("%Y-%m-%d"), 'answer':'n/a'})
        else:
             print("here")
             requests.post(URL_remove, json={'request_id': p.id_req})
             r = requests.post(URL_notifications, json={ 'request_id': p.id_req, 'description': description_removed + str(p.id_req), 'answer':"a"})
             print(r)
             
if __name__ == '__main__':
    patients = get_removed_requests()
    handle_removed_requests(patients)
