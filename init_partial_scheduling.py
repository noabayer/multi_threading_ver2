# -*- coding: utf-8 -*-
"""
Created on Sun Feb 28 11:34:21 2021

@author: dell
"""

import Patient_m
import requests
import datetime

def get_requests(status):
    patients = []
    URL = "https://www.api.orm-bgu-soroka.com/surgery_requests/get_all_by_ward_id_request_status"
    PARAMS = {"request_status": status,"ward_id":2}
    r = requests.post(url = URL, json =PARAMS) 
    data = r.json()
    for i in range(len(data)):
        pre_op_date =None
        schedule_date = None
        schedule_from = None
        schedule_deadline = None
        request_id = data[i]['request_id']
        unit, surgery_t = Patient_m.convert_code_name(data[i]['surgery_type_fk'])
        arrival = datetime.datetime.strptime(data[i]['entrance_date'], '%Y-%m-%d')
        if data[i]['pre_op_date'] !=None:
            pre_op_date = datetime.datetime.strptime(data[i]['pre_op_date'], '%Y-%m-%d')
        if data[i]['schedule_date'] != None:
            schedule_date = datetime.datetime.strptime(data[i]['schedule_date'], '%Y-%m-%d')
        if data[i]['schedule_from'] !=None:
            schedule_from = datetime.datetime.strptime(data[i]['schedule_from'], '%Y-%m-%d')
        if data[i]['schedule_deadline']!=None:
            schedule_deadline = datetime.datetime.strptime(data[i]['schedule_deadline'], '%Y-%m-%d')
        patients.append(Patient_m.Patient(data[i]['patient_id'],arrival,schedule_deadline,schedule_from, data[i]['specific_senior'], data[i]['urgency'], data[i]['complexity'], surgery_t, unit,schedule_date,data[i]['cancellations'],pre_op_date, request_id))
    return patients


def update_status_requests_surgery(patients_list,status):
    url = 'https://www.api.orm-bgu-soroka.com/surgery_requests/update_status'
    for patient in patients_list:
        requests.post(url,json={ 'request_id': patient.id_req, 'request_status':status})
                    

if __name__ == '__main__':
    patients_list = get_requests('1.2')
    update_status_requests_surgery(patients_list, '1.1')
