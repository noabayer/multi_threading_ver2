# -*- coding: utf-8 -*-
"""
Created on Tue Jan 19 18:38:58 2021

@author: dell
"""

import requests
import datetime
import Rooms
import Patient_m
import xlrd 
import pandas as pd
import numpy as np



def get_unit(date, room_id):
    if date.weekday() == 6: #6 - sunday
        if room_id == 1:
            return Rooms.Unit.Bariatric
        if room_id == 2:
            return Rooms.Unit.General
        return Rooms.Unit.Activity_Room
    if date.weekday() == 1:#1 is tuesday
        if room_id == 1:
            return Rooms.Unit.Colon
        return Rooms.Unit.Hospitalization_Day   
    if date.weekday() == 2: #2 is wednesday
        if room_id == 1:
            return Rooms.Unit.General
        return Rooms.Unit.Colon
    if date.weekday() == 3: #3 is thursday
        if room_id == 1:
            return Rooms.Unit.General
        return Rooms.Unit.Proctology 
    return -1



def initialize_data_base(path_partial_schedule, path_request_need_to_schedule) :
    patients_s = update_patients_APRIL(path_partial_schedule)
    patients_need_s = update_patients_APRIL(path_request_need_to_schedule)
    i = set_request_to_data_base(patients_s, "1.3", 20)
    set_request_to_data_base(patients_need_s, "1.1", i)

def get_requests(status):
    patients = {}
    URL = "https://www.api.orm-bgu-soroka.com/surgery_requests/get_all_by_ward_id_request_status"
    PARAMS = {"request_status": status,"ward_id":2}
    r = requests.post(url = URL, json =PARAMS) 
    data = r.json()
    for i in range(len(data)):
        pre_op_date =None
        schedule_date = None
        schedule_from = None
        schedule_deadline = None
        request_id = data[i]['request_id']
        #if (19 <int(request_id) and int(request_id)<200):
        unit, surgery_t = Patient_m.convert_code_name(data[i]['surgery_type_fk'])
        arrival = datetime.datetime.strptime(data[i]['entrance_date'], '%Y-%m-%d')
        if data[i]['pre_op_date'] !=None:
            pre_op_date = datetime.datetime.strptime(data[i]['pre_op_date'], '%Y-%m-%d')
        if data[i]['schedule_date'] != None:
            schedule_date = datetime.datetime.strptime(data[i]['schedule_date'], '%Y-%m-%d')
        if data[i]['schedule_from'] !=None:
            schedule_from = datetime.datetime.strptime(data[i]['schedule_from'], '%Y-%m-%d')
        if data[i]['schedule_deadline']!=None:
            schedule_deadline = datetime.datetime.strptime(data[i]['schedule_deadline'], '%Y-%m-%d')
        if unit not in patients:
            patients[unit] = []
        patients[unit].append( Patient_m.Patient(data[i]['patient_id'],arrival,schedule_deadline,schedule_from, data[i]['specific_senior'], data[i]['urgency'], data[i]['complexity'], surgery_t, unit,schedule_date,data[i]['cancellations'],pre_op_date, request_id))
    
    return patients

def get_requests_agent(status):
    patients = {}
    patients = {}
    patients[Rooms.Unit.Bariatric] = {}
    patients[Rooms.Unit.Activity_Room] = {}
    patients[Rooms.Unit.Proctology] = {}
    patients[Rooms.Unit.Colon] = {}
    patients[Rooms.Unit.Hospitalization_Day] = {}
    patients[Rooms.Unit.General] = {}
    URL = "https://www.api.orm-bgu-soroka.com/surgery_requests/get_all_by_ward_id_request_status"
    PARAMS = {"request_status": status,"ward_id":2}
    r = requests.post(url = URL, json =PARAMS) 
    data = r.json()
    for i in range(len(data)):
        pre_op_date =None
        schedule_date = None
        schedule_from = None
        schedule_deadline = None
        request_id = data[i]['request_id']
        id_p = data[i]['patient_id']
        unit, surgery_t = Patient_m.convert_code_name(data[i]['surgery_type_fk'])
        arrival = datetime.datetime.strptime(data[i]['entrance_date'], '%Y-%m-%d')
        if data[i]['pre_op_date'] !=None:
            pre_op_date = datetime.datetime.strptime(data[i]['pre_op_date'], '%Y-%m-%d')
        if data[i]['schedule_date'] != None:
            schedule_date = datetime.datetime.strptime(data[i]['schedule_date'], '%Y-%m-%d')
        if data[i]['schedule_from'] !=None:
            schedule_from = datetime.datetime.strptime(data[i]['schedule_from'], '%Y-%m-%d')
        if data[i]['schedule_deadline']!=None:
            schedule_deadline = datetime.datetime.strptime(data[i]['schedule_deadline'], '%Y-%m-%d')
        if unit not in patients:
            patients[unit] = []
        patients[unit][float(id_p)]  = ( Patient_m.Patient(id_p,arrival,schedule_deadline,schedule_from, data[i]['specific_senior'], data[i]['urgency'], data[i]['complexity'], surgery_t, unit,schedule_date,data[i]['cancellations'],pre_op_date, request_id))
    
    return patients    

def getting_room_allocation_for_surgical():
    id_surgical = 2
    rooms = {}
    URL = "https://www.api.orm-bgu-soroka.com/rooms_allocations/get_by_ward_id"
    PARAMS = {"ward_id": id_surgical}
    r = requests.post(url = URL, json =PARAMS) 
    data = r.json()
    for i in range(len(data)):
        date_time_obj = datetime.datetime.strptime(data[i]['date'], '%Y-%m-%d')
        room_id = data[i]['room_id']
        unit = get_unit(date_time_obj, room_id)
        if  date_time_obj not in rooms.keys():
            rooms[date_time_obj] = []
        rooms[date_time_obj].append(Rooms.Rooms(room_id, unit))                    
    return rooms, rooms.keys()


def create_dict_patient(path):
    patients = {}
    loc = (path) 
  
    wb = xlrd.open_workbook(loc) 
    sheet = wb.sheet_by_index(0)
  
    for i in range(1,sheet.nrows):
        id_p = sheet.cell_value(i, 7)
        a = sheet.cell_value(i, 6)
        d = sheet.cell_value(i, 5)
        f = sheet.cell_value(i, 10)
        pre_op = sheet.cell_value(i, 9)
        arrival = datetime.datetime(*xlrd.xldate_as_tuple(a, wb.datemode))
        if len(str(d))>0:
            d = datetime.datetime(*xlrd.xldate_as_tuple(d, wb.datemode))
        if len(str(f))>0:
            f = datetime.datetime(*xlrd.xldate_as_tuple(f, wb.datemode))
        if len(str(pre_op))>0:
            pre_op = datetime.datetime(*xlrd.xldate_as_tuple(pre_op, wb.datemode))
        else:
            pre_op = None
        surgeon = sheet.cell_value(i, 4)
        urgency = sheet.cell_value(i, 3)
        complexity = sheet.cell_value(i, 2)
        surgery_t = sheet.cell_value(i, 1)
        unit = sheet.cell_value(i, 0)
        num_cancellation = sheet.cell_value(i, 11)
        scheduling_date = sheet.cell_value(i, 12)
        if len(str(scheduling_date))>0:
            scheduling_date = datetime.datetime(*xlrd.xldate_as_tuple(scheduling_date, wb.datemode))
        else:
            scheduling_date = None
        if Rooms.convert_name_code(unit) not in patients:
            patients[Rooms.convert_name_code(unit)] = []
        patients[Rooms.convert_name_code(unit)].append( Patient_m.Patient(id_p,arrival,d,f, surgeon, urgency, complexity, surgery_t, unit,scheduling_date,num_cancellation,pre_op))
    return patients
        
#send patients to the database
def set_request_to_data_base(patients,status,i):
    URL_req = "https://www.api.orm-bgu-soroka.com/surgery_requests/add_surgery_request"
    #i = 20
#loop over all the patients
    for unit_p in patients:
        for p in patients[unit_p]:
            print(p.id_p)
            surgeon = None
            pre_op = None
            schedule_date = None
            st_time = p.arrival.strftime("%Y-%m-%d %H:%M:%S")
            deadline_time = None
            from_date_time = None
            #print(len(str(p.deadline)))
            if len(str(p.deadline))>10 :
                deadline_time = p.deadline.strftime("%Y-%m-%d %H:%M:%S")
            if len(str(p.from_date))>10:
                from_date_time = p.from_date.strftime("%Y-%m-%d %H:%M:%S")
            if p.pre_operation_date != None:
                pre_op = p.pre_operation_date.strftime("%Y-%m-%d %H:%M:%S")
            p.set_id_req(i)
            if (len(str(p.surgeon))>10):
                surgeon = str(p.surgeon)
            if p.scheduling_date !=None:
                schedule_date = p.scheduling_date.strftime("%Y-%m-%d %H:%M:%S")
			#send request for adding a new request_surgery to the data base	
            r = requests.post(URL_req, json={ 'request_id': i, 'patient_id':p.id_p,'surgery_type_fk':Patient_m.convert_name_code(p.unit, p.surgery_t), 'urgency': p.urgency,"complexity": p.complexity,'duration':60, 'request_status':status,'cancellations':p.num_cancelation,'entrance_date': st_time,"specific_senior":surgeon,"pre_op_date":pre_op,"schedule_date":schedule_date,"schedule_from":from_date_time, "schedule_deadline":deadline_time,"description":"surgery was added"})
            i = i+1
    return i
             
def fill_roomallocation(patients, rooms_allocations):
    for unit_p in patients:
        for p in patients[unit_p]:
            #if p.scheduling_date != None:
                rooms = rooms_allocations[p.scheduling_date]
                for room in rooms:
                    if room.unit1 == p.unit:
                        room.add_patient(p)
    return rooms_allocations

          
def send_to_partial_scheduling_data_base(allocation_rooms, flag = False):
    url = 'https://www.api.orm-bgu-soroka.com/surgeries_surgical_noas/add_surgery'
    for date in allocation_rooms:
        d = date
        for room in allocation_rooms[date]:
            #date_template = d.strftime("%Y-%m-%d")
            date_template = None
            for patient in room.patients:
                if patient.is_scheduled() == False:
                    st_time = d.strftime("%Y-%m-%d %H:%M:%S")
                    d = d+ datetime.timedelta(hours=2)
                    end_time = d.strftime("%Y-%m-%d %H:%M:%S")
                    description = str(patient.id_p)+" "+str(patient.surgery_t)+" C:"+str(patient.complexity)+", D:"+str(patient.deadline)+" Pre D: "+str(patient.pre_operation_date)+", arrival:"+str(patient.arrival)
                    r = requests.post(url, json={ 'request_id': patient.id_req, 'description': description, 'st_time': st_time, 'end_time': end_time, 'room_id': room.num, 'date':date_template,'resident_id': patient.id_p,'color':'#F7F171','removed':'f'})
    if flag:
        update_status_requests_surgery(allocation_rooms, '1.2')
        


def update_status_requests_surgery(allocation_rooms,status):
    url = 'https://www.api.orm-bgu-soroka.com/surgery_requests/update_status'
    for date in allocation_rooms:
        for room in allocation_rooms[date]:
            for patient in room.patients:
                if patient.is_scheduled() == False:
                    requests.post(url,json={ 'request_id': patient.id_req, 'request_status':status})
                    
                    
#writing result to excel file                   
def send_to_file(allocation_rooms):
    file_name = "D:\master\Research\multiple_client\data\data_output.xlsx"
    df = pd.read_excel(file_name) #Read Excel file as a DataFrame
    for date in allocation_rooms:
        for room in allocation_rooms[date]:
            for patient in room.patients:
                row_num = df[df['id'] == int(patient.id_p)].index[0]
                df.iloc[row_num, df.columns.get_loc('schedule')] = date
                df.iloc[row_num, df.columns.get_loc('pre op date')] = patient.pre_operation_date
    df.to_excel('D:\master\Research\multiple_client\data\output2.xlsx', engine='xlsxwriter') 


    
    
def convert_date_april(date):
    if int(date.day != 1):
        date = date - datetime.timedelta(days=1)
    return datetime.date(2021, 4, int(date.day))

def convert__arrival_date_april(date):
    date = date + datetime.timedelta(days=30*8)
    return date

#fixing data for APRIL-21, output - excel file           
def update_patients_APRIL(path):
    patients = {}
    file_name = path
    df = pd.read_excel(file_name) #Read Excel file as a DataFrame
    for index, row in df.iterrows():
        row_num = df.index.get_loc(index)
        #print(row_num)
        id_p = row['id']
        arrival = row['date']
        d = row['deadline']
        f = row['from']
        pre_op = row['pre op date']
        scheduling_date = row['schedule']
        arrival  = convert__arrival_date_april(arrival.to_pydatetime())
        #print(arrival)
        if len(str(d))>3:
            d = convert_date_april(d.to_pydatetime())
            #df.iloc[row_num, df.columns.get_loc('deadline')] = d
            #print(d)
        else:
            d = None
        if len(str(f))>3 :
            f = convert_date_april(f.to_pydatetime())
            #df.iloc[row_num, df.columns.get_loc('from')] = f
            #print(f)
        else:
            f = None
        if len(str(pre_op))>3:
            pre_op = convert_date_april(pre_op.to_pydatetime())
            #df.iloc[row_num, df.columns.get_loc('pre op date')] = pre_op
            #print(pre_op)
        else:
            pre_op = None
            
        if len(str(scheduling_date))>3:
            scheduling_date = convert_date_april(scheduling_date.to_pydatetime())
            #df.iloc[row_num, df.columns.get_loc('schedule')] = scheduling_date  
        else:
            scheduling_date = None
            
        surgeon = row['surgeon']
        urgency = row['urgency[1,3]']
        complexity = row['complexity[1,3]']
        surgery_t = row['surgery type']
        unit = row['unit']
        num_cancellation = row['cancelation']
        if Rooms.convert_name_code(unit) not in patients:
            patients[Rooms.convert_name_code(unit)] = []
        patients[Rooms.convert_name_code(unit)].append( Patient_m.Patient(id_p,arrival,d,f, surgeon, urgency, complexity, surgery_t, unit,scheduling_date,num_cancellation,pre_op))
    return patients
    #df.to_excel(b'D:\master\Research\multiple_client\data\april\output1.xlsx', engine='xlsxwriter') 
        
 
    
def update_pre_op_dates_requests_surgery(allocation_rooms):
    url = 'https://www.api.orm-bgu-soroka.com/surgery_requests/update_pre_op_date'
    for date in allocation_rooms:
        for room in allocation_rooms[date]:
            for patient in room.patients:
                pre_op_date = patient.pre_operation_date.strftime("%Y-%m-%d %H:%M:%S")
                r = requests.post(url,json={ 'request_id': patient.id_req, 'pre_op_date':pre_op_date})
                #print(r.json())
    
    
    
    
    
    # for date in allocation_rooms:
    #     for room in allocation_rooms[date]:
    #         for patient in room.patients:
    #             row_num = df[df['id'] == int(patient.id_p)].index[0]
    #             df.iloc[row_num, df.columns.get_loc('schedule')] = date
    #             df.iloc[row_num, df.columns.get_loc('pre op date')] = patient.pre_operation_date
    # df.to_excel('D:\master\Research\multiple_client\data\output2.xlsx', engine='xlsxwriter') 
            
#param: scheduling rooms , send this allocation to data base
def send_scheduling_data_base(allocation_rooms):
    url = ' https://www.api.orm-bgu-soroka.com/surgeries_surgical/add_surgery'
    for date in allocation_rooms:
        d = date
        for room in allocation_rooms[date]:
            for patient in room.patients:
                st_time = d.strftime("%Y-%m-%d %H:%M:%S")
                d = d+ datetime.timedelta(hours=1)
                end_time = d.strftime("%Y-%m-%d %H:%M:%S")
                description = str(patient.id_p)+" "+str(patient.surgery_t)+" C:"+str(patient.complexity)+", D:"+str(patient.deadline)+" Pre D: "+str(patient.pre_operation_date)+", arrival:"+str(patient.arrival)
                r = requests.post(url, json={ 'request_id': patient.id_req, 'description': description, 'st_time': st_time, 'end_time': end_time, 'room_id': room.num})
                #print(r.json())

def update_surgeries_table():
    #initialize_data_base("D:\master\Research\multiple_client\data\partial_scheduled.xlsx", "D:\master\Research\multiple_client\data\data_bariatric.xlsx")
    patients = get_requests("1.3")
    rooms_allocations, dates = getting_room_allocation_for_surgical()
    rooms_allocations = fill_roomallocation(patients,rooms_allocations)
    for date in rooms_allocations:
        for room in rooms_allocations[date]:
            #if room.unit1 == Rooms.convert_name_code('Bariatric'):
                print("date: ", date)
                for p in room.patients:
                    print(p.id_p, " Scheduled: ", p.is_scheduled(), " ",p.scheduling_date )
    
   
    send_scheduling_data_base(rooms_allocations)



#if __name__ == '__main__':
    #p = update_patients_APRIL("D:\master\Research\multiple_client\data\partial_scheduled.xlsx")
    #set_request_to_data_base(p,1.2,20)
    #update_surgeries_table()
    
   
    #send_scheduling_data_base(rooms_allocations)

    
    
    
    
    
    
    
    