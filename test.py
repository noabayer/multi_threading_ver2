# -*- coding: utf-8 -*-
"""
Created on Tue Sep 15 16:43:39 2020

@author: dell
"""

import threading
from threading import Thread 
import Classes
import Rooms
import Patient_m
from queue import Queue
import openpyxl



class Main(Thread):
    global_queue = Queue()
    def __init__(self,cvM):
        #self.cv1 = cv1
        self.cvM = cvM
    # A thread that produces dates for requests according the constraints of the agent
    # def producer(self, agent): 
    #     agent.
        #agent.producer(requests)
    
    def run(self):
        while True:
            msg = self.global_queue.get()
            print("get msg:", msg)
            print("handle task")
            self.queue_how.task_done()

    #def coordinator_p(self,coordinator,requests):
        # self.que.put(coordinator.producer(requests))
           
    def writing_excel_file(self,result):
        # Create the workbook and sheet for Excel
        workbook = openpyxl.Workbook()
        sheet = workbook.active
        
        # openpyxl does things based on 1 instead of 0
        row = 1
        for key,values in result.items():
            # Put the key in the first column for each key in the dictionary
            sheet.cell(row=row, column=1, value=key)
            column = 2
            for element in values:
                # Put the element in each adjacent column for each element in the tuple
                sheet.cell(row=row, column=column, value=element.__str__())
                column += 1
            row += 1
        workbook.save(filename="D:\master\Research\Model\my_workbook.xlsx")  





if __name__ == '__main__':
    #loading allocations rooms
    # main = Main()
    # allocation_rooms, __ = Rooms.create_rooms_allocation()
    # #loading list of request
    # #requests = Patient_m.create_dict_patient("D:\master\Research\multiple_client\data\data_bariatric.xlsx")
    # head_ward = Classes.Head_ward(2)
    # management= Classes.Management(2,allocation_rooms)
    # coordinator = Classes.Coordinator(allocation_rooms, list(allocation_rooms.keys()))
    # allocation_rooms_unit = management.get_dates_by_unit()
    #condition = threading.Condition()
    conditionM = threading.Condition()
    #agent - Coordinator
    #t1 = Thread(target=lambda q, arg1: q.put(coordinator_p(arg1)), args=(coordinator,requests, ))
    #t1 = Thread(target = main.coordinator_p, args =(coordinator,requests, ))
    t = Main(conditionM)
    t1 = Classes.Coordinator(conditionM)
    t2 = Classes.Head_ward(2,conditionM)
    t.run()
    t1.run()
    t2.run()
    
    
    
    
    
    
    #agent - Head Ward
    # t2 = Thread(target = main.producer, args =(head_ward, requests)) 
    # #agent - Management
    # t3 = Thread(target = main.producer, args =(management, requests)) 
    
    # t2.start() 
    # t3.start()
    
    # #After Head Ward and Management agents finish to set the dates
    # t2.join()
    # t3.join()
    # #Agent Coordinator start working
    # t1.start()
    # t1.join()
    # result = main.que.get()
    # #writing_excel_file(result)
    # #allocation_rooms = coordinator.allocation_rooms
    # dates = list(allocation_rooms.keys())
    # patients_specific_date = coordinator.set_option_for_room(dates[1], allocation_rooms[dates[1]][0])
    # print(patients_specific_date)

    


        
        
        
        
        
        
        
        